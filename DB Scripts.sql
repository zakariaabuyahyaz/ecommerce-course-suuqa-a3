create database suuqaa3
GO
use suuqaa3
GO

--- creating table structure
create table tbl_<name>
(
id bigint primary key,

is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime

)


create table tbl_users
(
id bigint primary key,
full_name varchar(250) not null,
mobile varchar(50) unique not null,
email varchar(250) unique not null,
is_blocked bit,
block_date date,
[password] varchar(max),
gender varchar(15),
date_of_brith date,
country varchar(100),
city varchar(100),
[address] varchar(max),
user_type varchar(50) not null,
marital_status varchar(100),
is_second_factor_enable bit,
user_image varchar(max),
is_deleted bit,
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime
)


create table tbl_category
(
id bigint primary key,
name varchar(250) unique not null,
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime
)

create table tbl_vendors
(
id bigint primary key,
name varchar(250) unique,
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime
)


create table tbl_sales_status
(
id bigint primary key,
name varchar(150) unique, 
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime

)

create table tbl_payment_types
(
id bigint primary key,
name varchar(150) unique, 
is_online bit,
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime

)

create table tbl_login_history
(
id bigint primary key,
[user_id] bigint foreign key references tbl_users(id),
login_date_time smalldatetime,
is_deleted bit,
deleted_by varchar(50)
)

create table tbl_products
(
id bigint primary key,
name varchar(250) unique,
category_id bigint foreign key references tbl_category(id),
barcode varchar(250) unique,
remarks varchar(max),
manufactured_country varchar(100),
manufacturer varchar(150),
brand_name varchar(150),
expire_date date,
manufacture_date date,
quantity_onhand float,
selling_price decimal(18,2),
cost_price decimal(18,2),
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime
)



create table tbl_purchase
(
id bigint primary key,
vendor_id bigint foreign key references tbl_vendors(id),
purchase_date date,
product_id bigint foreign key references tbl_products(id),
qty float,
cost_price decimal(18,2),
selling_price decimal(18,2),
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime
)



create table tbl_sales
(
id bigint primary key,
[user_id]  bigint foreign key references tbl_users(id),
product_id  bigint foreign key references tbl_products(id),
price decimal(18,2),
qty float,
total_price decimal(18,2),
discount decimal(18,2),
net_amount decimal(18,2),
sales_date date,
payment_type_id bigint foreign key references tbl_payment_types(id),
sales_status_id bigint foreign key references tbl_sales_status(id),
verified_by bigint foreign key references tbl_users(id),
is_deleted bit,
deleted_by varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime
)


create table tbl_product_images
(
id bigint primary key,
product_id bigint foreign key references tbl_products(id),
[type] varchar(100),
[path] varchar(max),
is_deleted bit,
deleted_by varchar(50),
ruser varchar(50),
rdate smalldatetime,
muser varchar(50),
mdate smalldatetime

)


select * from sys.tables 

-------------------------- SEEDING----------------------

INSERT INTO tbl_users (id, full_name, mobile, email, is_blocked, block_date, [password], gender, date_of_brith, country, city, [address], user_type, marital_status, is_second_factor_enable, user_image, is_deleted, ruser, rdate, muser, mdate)
VALUES 
    (1, 'Ali Jama', '252634445566', 'john@example.com', 0, NULL, '123456', 'Male', '1990-01-01', 'USA', 'New York', '123 ABC Street', 'client', 'Single', 0, 'image1.jpg', 0, null, GETDATE(), null,null),
    (2, 'Cusman Ahmed', '252634442233', 'jane@example.com', 0, NULL, '123456', 'Male', '1995-05-10', 'Canada', 'Toronto', '456 XYZ Street', 'admin', 'Married', 0, 'image2.jpg', 0, null, GETDATE(), null,null)


	INSERT INTO tbl_category (id, name, is_deleted, deleted_by, ruser, rdate, muser, mdate)
VALUES 
    (1, 'Clothes', 0, NULL, 'jane@example.com', GETDATE(), null,null),
    (2, 'Electronics', 0, NULL, 'jane@example.com', GETDATE(), null, null)


	INSERT INTO tbl_vendors (id, name, is_deleted, deleted_by, ruser, rdate, muser, mdate)
VALUES 
    (1, 'China Vendor One', 0, NULL, 'jane@example.com', GETDATE(), null,null),
    (2, 'Turkey Vendor Two', 0, NULL, 'jane@example.com', GETDATE(), null,null)

	INSERT INTO tbl_sales_status (id, name, is_deleted, deleted_by, ruser, rdate, muser, mdate)
VALUES 
    (1, 'New Order', 0, NULL, 'jane@example.com', GETDATE(), null,null),
    (2, 'Verified', 0, NULL, 'jane@example.com', GETDATE(), null,null),
	(3, 'On the way', 0, NULL, 'jane@example.com', GETDATE(), null,null),
	(4, 'Completed', 0, NULL, 'jane@example.com', GETDATE(), null,null)


	INSERT INTO tbl_payment_types (id, name, is_online, is_deleted, deleted_by, ruser, rdate, muser, mdate)
VALUES 
    (1, 'Zaad', 1, 0, NULL, 'jane@example.com', GETDATE(), null,null),
    (2, 'Edahab', 1, 0, NULL, 'jane@example.com', GETDATE(), null,null),
	(3, 'On Delivery', 0, 0, NULL, 'jane@example.com', GETDATE(), null,null)


	-----------------------------VIEWS-------------------------------

	select * from tbl_category
	select * from tbl_products
	select * from tbl_product_images

	create view v_products
	as
	select b.id categoryid, b.name category, a.*, c.path
	from tbl_products a with(nolock) join tbl_category b with(nolock)
	on(a.category_id=b.id) left join tbl_product_images c with(nolock)
	on(a.id=c.product_id and c.type='Primary')
	where a.is_deleted=0

	select * from tbl_category
	select * from tbl_products
	select * from tbl_sales_status
	select * from tbl_payment_types
	select * from tbl_sales

	create view v_sales
	as
	select a.*, b.name products_name, b.barcode products_barcode, b.cost_price products_cost_price, b.selling_price products_selling_price, 
	b.expire_date products_expire_Date, b.quantity_onhand products_qyt_onhand, c.name sales_status_name, d.name payment_type_name
	from tbl_sales a with(nolock) join tbl_products b with(nolock) 
	on a.product_id=b.id join tbl_category e 
	on b.category_id=e.id join tbl_sales_status c 
	on a.sales_status_id=c.id join tbl_payment_types d 
	on a.payment_type_id=d.id  
	where a.is_deleted=0
