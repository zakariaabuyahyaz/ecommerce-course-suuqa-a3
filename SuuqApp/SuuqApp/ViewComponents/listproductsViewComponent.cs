﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuuqApp.Data;

namespace SuuqApp.ViewComponents
{
    public class listproductsViewComponent : ViewComponent
    {
        private readonly suuqContext _context;

        public listproductsViewComponent(suuqContext context)
        {
            _context = context;
        }

        public IViewComponentResult Invoke()
        {
            var data = _context.TblProducts
                .Include(p => p.Category)
                .Include(p => p.TblProductImages)
                .Take(10)
                .ToList();
            return View(data);
        }
    }
}
