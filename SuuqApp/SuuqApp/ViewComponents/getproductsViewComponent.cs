﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuuqApp.Data;

namespace SuuqApp.ViewComponents
{
    public class getproductsViewComponent : ViewComponent
    {
        private readonly suuqContext _context;

        public getproductsViewComponent(suuqContext context)
        {
            _context = context;
        }

        public IViewComponentResult Invoke()
        {
            var data = _context.TblProducts.Include(p => p.Category).ToList();
            return View(data);
        }

    }
}
