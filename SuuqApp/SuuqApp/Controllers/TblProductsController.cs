﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SuuqApp.Data;
using SuuqApp.Models;

namespace SuuqApp.Controllers
{
    public class TblProductsController : Controller
    {
        private readonly suuqContext _context;

        public TblProductsController(suuqContext context)
        {
            _context = context;
        }

        // GET: TblProducts
        public async Task<IActionResult> Index()
        {
            var data = await _context.TblProducts
                .Include(p => p.Category)
                .OrderByDescending(p => p.Id)
                .ToListAsync();
            return View(data);
        }

        public async Task<IActionResult> ShowDetails(int id)
        {
            var data = await _context.TblProducts
                .Include(p => p.Category)
                .Include(p => p.TblProductImages)
                .Where(p => p.Id == id)
                .FirstAsync();  

            return View(data);
                
                
        }

        public async Task<IActionResult> FindItems()
        {
            var data = await _context.TblProducts
                .Include(p => p.Category)
                .Include(p => p.TblProductImages)
                .OrderBy(p => p.Name)
                .ToListAsync();

            var categoryCounts = _context.TblCategories
                .Select(category => new CategoryWithProductCountViewModel
                {
                    CategoryName = category.Name,
                    ProductCount = _context.TblProducts.Count(product => product.CategoryId == category.Id)
                })
                .ToList();

            ViewData["category"] = categoryCounts;
            return View(data);
        }

        public async Task<IActionResult> Images(int id)
        {
            var images = await _context.TblProductImages.Where(p => p.ProductId == id).ToListAsync();
            ViewBag.images = images;
            ViewData["productid"] = id;
            return View();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Images([Bind("ProductId,Type")] TblProductImage modal, IFormFile imagefile)
        {
            if (ModelState.IsValid)
            {
                //upload image
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/uploads", imagefile.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await imagefile.CopyToAsync(stream);
                }

                modal.Ruser = "zakaria";
                modal.Rdate = DateTime.Now;
                modal.Path = imagefile.FileName;
                modal.IsDeleted = false;

                _context.Add(modal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Images), new { id = modal.ProductId });
            }
            ViewData["productid"] = modal.ProductId;
            return View(modal);
        }

        // GET: TblProducts/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null || _context.TblProducts == null)
            {
                return NotFound();
            }

            var tblProduct = await _context.TblProducts
                .Include(t => t.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblProduct == null)
            {
                return NotFound();
            }

            return View(tblProduct);
        }

        // GET: TblProducts/Create
        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(_context.TblCategories, "Id", "Name");
            return View();
        }

        // POST: TblProducts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,CategoryId,Barcode,Remarks,ManufacturedCountry,Manufacturer,BrandName,ExpireDate,ManufactureDate,QuantityOnhand,SellingPrice,CostPrice")] TblProduct tblProduct)
        {
            if (ModelState.IsValid)
            {
                tblProduct.IsDeleted = false;
                tblProduct.Rdate = DateTime.Now;
                tblProduct.Ruser = "";
                tblProduct.DeletedBy = "";

                _context.Add(tblProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.TblCategories, "Id", "Name", tblProduct.CategoryId);
            return View(tblProduct);
        }

        // GET: TblProducts/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null || _context.TblProducts == null)
            {
                return NotFound();
            }

            var tblProduct = await _context.TblProducts.FindAsync(id);
            if (tblProduct == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_context.TblCategories, "Id", "Id", tblProduct.CategoryId);
            return View(tblProduct);
        }

        // POST: TblProducts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Name,CategoryId,Barcode,Remarks,ManufacturedCountry,Manufacturer,BrandName,ExpireDate,ManufactureDate,QuantityOnhand,SellingPrice,CostPrice,IsDeleted,DeletedBy,Ruser,Rdate,Muser,Mdate")] TblProduct tblProduct)
        {
            if (id != tblProduct.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblProduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblProductExists(tblProduct.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.TblCategories, "Id", "Id", tblProduct.CategoryId);
            return View(tblProduct);
        }

        // GET: TblProducts/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null || _context.TblProducts == null)
            {
                return NotFound();
            }

            var tblProduct = await _context.TblProducts
                .Include(t => t.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblProduct == null)
            {
                return NotFound();
            }

            return View(tblProduct);
        }

        // POST: TblProducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            if (_context.TblProducts == null)
            {
                return Problem("Entity set 'suuqContext.TblProducts'  is null.");
            }
            var tblProduct = await _context.TblProducts.FindAsync(id);
            if (tblProduct != null)
            {
                _context.TblProducts.Remove(tblProduct);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblProductExists(long id)
        {
            return (_context.TblProducts?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        public IActionResult getProductList()
        {
            var data = _context.TblProducts.Include(p => p.Category).ToList();
            return PartialView(data);
        }
    }

   

}
