﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_sales_status")]
[Index("Name", Name = "UQ__tbl_sale__72E12F1B722DC961", IsUnique = true)]
public partial class TblSalesStatus
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("name")]
    [StringLength(150)]
    [Unicode(false)]
    public string? Name { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [InverseProperty("SalesStatus")]
    public virtual ICollection<TblSale> TblSales { get; set; } = new List<TblSale>();
}
