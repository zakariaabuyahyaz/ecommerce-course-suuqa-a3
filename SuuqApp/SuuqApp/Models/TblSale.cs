﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_sales")]
public partial class TblSale
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("user_id")]
    public long? UserId { get; set; }

    [Column("product_id")]
    public long? ProductId { get; set; }

    [Column("price", TypeName = "decimal(18, 2)")]
    public decimal? Price { get; set; }

    [Column("qty")]
    public double? Qty { get; set; }

    [Column("total_price", TypeName = "decimal(18, 2)")]
    public decimal? TotalPrice { get; set; }

    [Column("discount", TypeName = "decimal(18, 2)")]
    public decimal? Discount { get; set; }

    [Column("net_amount", TypeName = "decimal(18, 2)")]
    public decimal? NetAmount { get; set; }

    [Column("sales_date", TypeName = "date")]
    public DateTime? SalesDate { get; set; }

    [Column("payment_type_id")]
    public long? PaymentTypeId { get; set; }

    [Column("sales_status_id")]
    public long? SalesStatusId { get; set; }

    [Column("verified_by")]
    public long? VerifiedBy { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [ForeignKey("PaymentTypeId")]
    [InverseProperty("TblSales")]
    public virtual TblPaymentType? PaymentType { get; set; }

    [ForeignKey("ProductId")]
    [InverseProperty("TblSales")]
    public virtual TblProduct? Product { get; set; }

    [ForeignKey("SalesStatusId")]
    [InverseProperty("TblSales")]
    public virtual TblSalesStatus? SalesStatus { get; set; }

    [ForeignKey("UserId")]
    [InverseProperty("TblSaleUsers")]
    public virtual TblUser? User { get; set; }

    [ForeignKey("VerifiedBy")]
    [InverseProperty("TblSaleVerifiedByNavigations")]
    public virtual TblUser? VerifiedByNavigation { get; set; }
}
