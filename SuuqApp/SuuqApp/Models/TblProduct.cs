﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_products")]
[Index("Name", Name = "UQ__tbl_prod__72E12F1B69886CC7", IsUnique = true)]
[Index("Barcode", Name = "UQ__tbl_prod__C16E36F8D6F10BDC", IsUnique = true)]
public partial class TblProduct
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("name")]
    [StringLength(250, MinimumLength =3)]
    [Unicode(false)]
    [Required]

    public string? Name { get; set; }

    [Column("category_id")]
    [Required(ErrorMessage ="The Category is required")]
    public long? CategoryId { get; set; }

    [Column("barcode")]
    [StringLength(250)]
    [Unicode(false)]
    public string? Barcode { get; set; }

    [Column("remarks")]
    [Unicode(false)]
    public string? Remarks { get; set; }

    [Column("manufactured_country")]
    [StringLength(100)]
    [Unicode(false)]
    public string? ManufacturedCountry { get; set; }

    [Column("manufacturer")]
    [StringLength(150)]
    [Unicode(false)]
    public string? Manufacturer { get; set; }

    [Column("brand_name")]
    [StringLength(150)]
    [Unicode(false)]
    public string? BrandName { get; set; }

    [Column("expire_date", TypeName = "date")]
    public DateTime? ExpireDate { get; set; }

    [Column("manufacture_date", TypeName = "date")]
    public DateTime? ManufactureDate { get; set; }

    [Column("quantity_onhand")]
    [Required]
    public double? QuantityOnhand { get; set; }

    [Column("selling_price", TypeName = "decimal(18, 2)")]
    [Required]
    public decimal? SellingPrice { get; set; }

    [Column("cost_price", TypeName = "decimal(18, 2)")]
    [Required]
    public decimal? CostPrice { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [ForeignKey("CategoryId")]
    [InverseProperty("TblProducts")]
    public virtual TblCategory? Category { get; set; }

    [InverseProperty("Product")]
    public virtual ICollection<TblProductImage> TblProductImages { get; set; } = new List<TblProductImage>();

    [InverseProperty("Product")]
    public virtual ICollection<TblPurchase> TblPurchases { get; set; } = new List<TblPurchase>();

    [InverseProperty("Product")]
    public virtual ICollection<TblSale> TblSales { get; set; } = new List<TblSale>();
}
