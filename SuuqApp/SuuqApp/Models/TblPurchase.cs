﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_purchase")]
public partial class TblPurchase
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("vendor_id")]
    public long? VendorId { get; set; }

    [Column("purchase_date", TypeName = "date")]
    public DateTime? PurchaseDate { get; set; }

    [Column("product_id")]
    public long? ProductId { get; set; }

    [Column("qty")]
    public double? Qty { get; set; }

    [Column("cost_price", TypeName = "decimal(18, 2)")]
    public decimal? CostPrice { get; set; }

    [Column("selling_price", TypeName = "decimal(18, 2)")]
    public decimal? SellingPrice { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [ForeignKey("ProductId")]
    [InverseProperty("TblPurchases")]
    public virtual TblProduct? Product { get; set; }

    [ForeignKey("VendorId")]
    [InverseProperty("TblPurchases")]
    public virtual TblVendor? Vendor { get; set; }
}
