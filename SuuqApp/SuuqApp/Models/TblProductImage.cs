﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_product_images")]
public partial class TblProductImage
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("product_id")]
    [Required]
    public long? ProductId { get; set; }

    [Column("type")]
    [StringLength(100)]
    [Unicode(false)]
    [Required]
    public string? Type { get; set; }

    [Column("path")]
    [Unicode(false)]
    public string? Path { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [ForeignKey("ProductId")]
    [InverseProperty("TblProductImages")]
    public virtual TblProduct? Product { get; set; }

}
