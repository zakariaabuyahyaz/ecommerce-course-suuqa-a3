﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_payment_types")]
[Index("Name", Name = "UQ__tbl_paym__72E12F1B2D0E60A5", IsUnique = true)]
public partial class TblPaymentType
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("name")]
    [StringLength(150)]
    [Unicode(false)]
    public string? Name { get; set; }

    [Column("is_online")]
    public bool? IsOnline { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [InverseProperty("PaymentType")]
    public virtual ICollection<TblSale> TblSales { get; set; } = new List<TblSale>();
}
