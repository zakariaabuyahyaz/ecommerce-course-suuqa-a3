﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_category")]
[Index("Name", Name = "UQ__tbl_cate__72E12F1B88DBBEC1", IsUnique = true)]
public partial class TblCategory
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("name")]
    [StringLength(250)]
    [Unicode(false)]
    public string Name { get; set; } = null!;

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [InverseProperty("Category")]
    public virtual ICollection<TblProduct> TblProducts { get; set; } = new List<TblProduct>();
}
