﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Keyless]
public partial class VProduct
{
    [Column("categoryid")]
    public long Categoryid { get; set; }

    [Column("category")]
    [StringLength(250)]
    [Unicode(false)]
    public string Category { get; set; } = null!;

    [Column("id")]
    public long Id { get; set; }

    [Column("name")]
    [StringLength(250)]
    [Unicode(false)]
    public string? Name { get; set; }

    [Column("category_id")]
    public long? CategoryId1 { get; set; }

    [Column("barcode")]
    [StringLength(250)]
    [Unicode(false)]
    public string? Barcode { get; set; }

    [Column("remarks")]
    [Unicode(false)]
    public string? Remarks { get; set; }

    [Column("manufactured_country")]
    [StringLength(100)]
    [Unicode(false)]
    public string? ManufacturedCountry { get; set; }

    [Column("manufacturer")]
    [StringLength(150)]
    [Unicode(false)]
    public string? Manufacturer { get; set; }

    [Column("brand_name")]
    [StringLength(150)]
    [Unicode(false)]
    public string? BrandName { get; set; }

    [Column("expire_date", TypeName = "date")]
    public DateTime? ExpireDate { get; set; }

    [Column("manufacture_date", TypeName = "date")]
    public DateTime? ManufactureDate { get; set; }

    [Column("quantity_onhand")]
    public double? QuantityOnhand { get; set; }

    [Column("selling_price", TypeName = "decimal(18, 2)")]
    public decimal? SellingPrice { get; set; }

    [Column("cost_price", TypeName = "decimal(18, 2)")]
    public decimal? CostPrice { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("deleted_by")]
    [StringLength(50)]
    [Unicode(false)]
    public string? DeletedBy { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [Column("path")]
    [Unicode(false)]
    public string? Path { get; set; }
}
