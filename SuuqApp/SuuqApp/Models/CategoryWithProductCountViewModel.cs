﻿namespace SuuqApp.Models
{
    public class CategoryWithProductCountViewModel
    {
        public string CategoryName { get; set; }
        public int ProductCount { get; set; }
    }
}
