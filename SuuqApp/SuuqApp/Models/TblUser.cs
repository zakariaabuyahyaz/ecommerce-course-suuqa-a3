﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SuuqApp.Models;

[Table("tbl_users")]
[Index("Mobile", Name = "UQ__tbl_user__A32E2E1CF9341049", IsUnique = true)]
[Index("Email", Name = "UQ__tbl_user__AB6E61643FEB4164", IsUnique = true)]
public partial class TblUser
{
    [Key]
    [Column("id")]
    public long Id { get; set; }

    [Column("full_name")]
    [StringLength(250)]
    [Unicode(false)]
    public string FullName { get; set; } = null!;

    [Column("mobile")]
    [StringLength(50)]
    [Unicode(false)]
    public string Mobile { get; set; } = null!;

    [Column("email")]
    [StringLength(250)]
    [Unicode(false)]
    public string Email { get; set; } = null!;

    [Column("is_blocked")]
    public bool? IsBlocked { get; set; }

    [Column("block_date", TypeName = "date")]
    public DateTime? BlockDate { get; set; }

    [Column("password")]
    [Unicode(false)]
    public string? Password { get; set; }

    [Column("gender")]
    [StringLength(15)]
    [Unicode(false)]
    public string? Gender { get; set; }

    [Column("date_of_brith", TypeName = "date")]
    public DateTime? DateOfBrith { get; set; }

    [Column("country")]
    [StringLength(100)]
    [Unicode(false)]
    public string? Country { get; set; }

    [Column("city")]
    [StringLength(100)]
    [Unicode(false)]
    public string? City { get; set; }

    [Column("address")]
    [Unicode(false)]
    public string? Address { get; set; }

    [Column("user_type")]
    [StringLength(50)]
    [Unicode(false)]
    public string UserType { get; set; } = null!;

    [Column("marital_status")]
    [StringLength(100)]
    [Unicode(false)]
    public string? MaritalStatus { get; set; }

    [Column("is_second_factor_enable")]
    public bool? IsSecondFactorEnable { get; set; }

    [Column("is_deleted")]
    public bool? IsDeleted { get; set; }

    [Column("ruser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Ruser { get; set; }

    [Column("rdate", TypeName = "smalldatetime")]
    public DateTime? Rdate { get; set; }

    [Column("muser")]
    [StringLength(50)]
    [Unicode(false)]
    public string? Muser { get; set; }

    [Column("mdate", TypeName = "smalldatetime")]
    public DateTime? Mdate { get; set; }

    [Column("user_image")]
    [Unicode(false)]
    public string? UserImage { get; set; }

    [InverseProperty("User")]
    public virtual ICollection<TblLoginHistory> TblLoginHistories { get; set; } = new List<TblLoginHistory>();

    [InverseProperty("User")]
    public virtual ICollection<TblSale> TblSaleUsers { get; set; } = new List<TblSale>();

    [InverseProperty("VerifiedByNavigation")]
    public virtual ICollection<TblSale> TblSaleVerifiedByNavigations { get; set; } = new List<TblSale>();
}
