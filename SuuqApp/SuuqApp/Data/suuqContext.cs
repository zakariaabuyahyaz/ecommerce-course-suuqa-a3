﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SuuqApp.Models;

namespace SuuqApp.Data;

public partial class suuqContext : DbContext
{
    public suuqContext()
    {
    }

    public suuqContext(DbContextOptions<suuqContext> options)
        : base(options)
    {
    }

    public virtual DbSet<TblCategory> TblCategories { get; set; }

    public virtual DbSet<TblLoginHistory> TblLoginHistories { get; set; }

    public virtual DbSet<TblPaymentType> TblPaymentTypes { get; set; }

    public virtual DbSet<TblProduct> TblProducts { get; set; }

    public virtual DbSet<TblProductImage> TblProductImages { get; set; }

    public virtual DbSet<TblPurchase> TblPurchases { get; set; }

    public virtual DbSet<TblSale> TblSales { get; set; }

    public virtual DbSet<TblSalesStatus> TblSalesStatuses { get; set; }

    public virtual DbSet<TblUser> TblUsers { get; set; }

    public virtual DbSet<TblVendor> TblVendors { get; set; }

    public virtual DbSet<VProduct> VProducts { get; set; }

    public virtual DbSet<VSale> VSales { get; set; }

//    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//        => optionsBuilder.UseSqlServer("server=localhost; initial catalog=suuqaa3; user id=sa; password=per457@.Q;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TblCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_cate__3213E83F6D7DA17D");

            entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<TblLoginHistory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_logi__3213E83FD518E23A");

            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.HasOne(d => d.User).WithMany(p => p.TblLoginHistories).HasConstraintName("FK__tbl_login__user___1FCDBCEB");
        });

        modelBuilder.Entity<TblPaymentType>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_paym__3213E83FC56D005C");

            entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<TblProduct>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_prod__3213E83FB9B53C23");

            entity.Property(e => e.Id).ValueGeneratedOnAdd();

            entity.HasOne(d => d.Category).WithMany(p => p.TblProducts).HasConstraintName("FK__tbl_produ__categ__24927208");
        });

        modelBuilder.Entity<TblProductImage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_prod__3213E83FF1C58475");

            entity.Property(e => e.Id).ValueGeneratedOnAdd();

            entity.HasOne(d => d.Product).WithMany(p => p.TblProductImages).HasConstraintName("FK__tbl_produ__produ__31EC6D26");
        });

        modelBuilder.Entity<TblPurchase>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_purc__3213E83F9580E9ED");

            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.HasOne(d => d.Product).WithMany(p => p.TblPurchases).HasConstraintName("FK__tbl_purch__produ__286302EC");

            entity.HasOne(d => d.Vendor).WithMany(p => p.TblPurchases).HasConstraintName("FK__tbl_purch__vendo__276EDEB3");
        });

        modelBuilder.Entity<TblSale>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_sale__3213E83F5DC57774");

            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.HasOne(d => d.PaymentType).WithMany(p => p.TblSales).HasConstraintName("FK__tbl_sales__payme__2D27B809");

            entity.HasOne(d => d.Product).WithMany(p => p.TblSales).HasConstraintName("FK__tbl_sales__produ__2C3393D0");

            entity.HasOne(d => d.SalesStatus).WithMany(p => p.TblSales).HasConstraintName("FK__tbl_sales__sales__2E1BDC42");

            entity.HasOne(d => d.User).WithMany(p => p.TblSaleUsers).HasConstraintName("FK__tbl_sales__user___2B3F6F97");

            entity.HasOne(d => d.VerifiedByNavigation).WithMany(p => p.TblSaleVerifiedByNavigations).HasConstraintName("FK__tbl_sales__verif__2F10007B");
        });

        modelBuilder.Entity<TblSalesStatus>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_sale__3213E83F955AF12B");

            entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<TblUser>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_user__3213E83F89ABDCD6");

            entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<TblVendor>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tbl_vend__3213E83F9BC28790");

            entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<VProduct>(entity =>
        {
            entity.ToView("v_products");
        });

        modelBuilder.Entity<VSale>(entity =>
        {
            entity.ToView("v_sales");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
